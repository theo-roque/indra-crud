import { combineReducers } from "redux";
import storage from "redux-persist/lib/storage";

//  Importing reducers
import { reducer as form } from "redux-form";
import app from "../screens/App/App.reducer";
import notificationManager from "../utils/NotificationManager/NotificationManager.reducer";

//  Combining all existing reducers
const appReducer = combineReducers({
    app,
    notificationManager,
    form
});

const rootReducer = (state, action) => {
    if (action.type === "RESET_STATE") {
        storage.removeItem("persist:root");
        state = undefined;
    }

    return appReducer(state, action);
};

export default rootReducer;
