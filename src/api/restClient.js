import axios from "axios";

/**
 * Preparing rest client for react app
 * Here should be all default params defined
 */
// Set the token here since this is only a test (no prod)
const restClient = axios.create({
    baseURL: "https://reqres.in/api"
});

// handle generic events - like loading and 500 type errors - in API interceptors
restClient.interceptors.request.use(config => {
    // display a single subtle loader on the top of the page when there is networking in progress
    // avoid multiple loaders, use placeholders or consistent updates instead
    return config;
});

export default restClient;
