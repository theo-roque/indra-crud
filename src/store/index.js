import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import combinedReducers from "../reducers";

/**
|--------------------------------------------------
| Put reducers to local storage
| Do not store form to local storage
|--------------------------------------------------
*/

/**
|--------------------------------------------------
| Put reducers to local storage
| Do not store form to local storage
|--------------------------------------------------
*/
const reducer = persistReducer({ key: "state", storage }, combinedReducers);

const middleware = applyMiddleware(thunk);
//  Prepare store for Provider
const store = createStore(reducer, middleware);

//  Prepare persistor for persistStore
const persistor = persistStore(store);

//  Exporting store and persistor for providers in index
export { store, persistor };
