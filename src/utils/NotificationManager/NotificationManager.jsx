import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import Alert from "@material-ui/lab/Alert";

const useStyles = makeStyles(theme => ({
    root: {
        marginBottom: theme.spacing(2)
    }
}));

/**
 * Notification Manager
 * @param code
 * @returns {*}
 * @constructor
 */
const NotificationManager = ({ pageNotification }) => {
    const classes = useStyles();
    switch (pageNotification.code) {
        case "added":
            return (
                <Alert severity="success" className={classes.root}>
                    User successfully added:{" "}
                    {`${pageNotification.data.first_name} ${pageNotification.data.last_name}`}
                </Alert>
            );
        case "deleted":
            return (
                <Alert severity="success" className={classes.root}>
                    User successfully deleted!
                </Alert>
            );
        case "updated":
            return (
                <Alert severity="success" className={classes.root}>
                    User successfully updated:{" "}
                    {`${pageNotification.data.first_name} ${pageNotification.data.last_name}`}
                </Alert>
            );
        case "error":
            return (
                <Alert className={classes.root} severity="error">
                    Something went wrong.
                </Alert>
            );
        default:
            return (
                <Alert className={classes.root} severity="error">
                    Something went wrong.
                </Alert>
            );
    }
};

NotificationManager.defaultProps = {
    pageNotification: null
};

NotificationManager.propTypes = {
    pageNotification: PropTypes.object
};

export default NotificationManager;
