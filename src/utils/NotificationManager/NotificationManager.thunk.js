import { actions as notificationManagerActions } from "./NotificationManager.reducer";

export const showNotification = props => {
    return dispatch => {
        dispatch(showNotificationStarted(props));
    };
};

export const removeNotification = () => {
    return dispatch => {
        dispatch(removeNotificationStarted());
    };
};

const showNotificationStarted = response => ({
    type: notificationManagerActions.SET_PAGE_NOTIFICATION,
    response
});

const removeNotificationStarted = () => ({
    type: notificationManagerActions.REMOVE_PAGE_NOTIFICATION
});
