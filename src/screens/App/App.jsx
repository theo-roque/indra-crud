import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import MaterialTable from "material-table";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";
import LinearProgress from "@material-ui/core/LinearProgress";
import Avatar from "@material-ui/core/Avatar";
import { getListingData, getFetching } from "./App.selectors";
import { getPageNotification } from "../../utils/NotificationManager/NotificationManager.selectors";
import NotificationManager from "../../utils/NotificationManager";
import {
    fetchListingData,
    addTableRow,
    updateTableRow,
    deleteTableRow
} from "./App.thunk";
import tableIcons from "../../utils/tableIcons";

const useStyles = makeStyles({
    container: {
        minWidth: "1000px"
    }
});

const App = ({ dispatch, fetching, listingData, pageNotification }) => {
    const classes = useStyles();
    const [table, setTable] = useState([]);
    const [showNotif, setShowNotif] = useState(false);
    const columns = [
        { title: "ID", field: "id", editable: "never" },
        {
            title: "Avatar",
            field: "avatar",
            render: rowData => (
                <Avatar
                    src={rowData && "avatar" in rowData ? rowData.avatar : ""}
                    alt={
                        rowData && "first_name" in rowData
                            ? rowData.first_name
                            : ""
                    }
                />
            )
        },
        { title: "First Name", field: "first_name" },
        { title: "Last Name", field: "last_name" },
        { title: "Email", field: "email" }
    ];

    useEffect(() => {
        dispatch(fetchListingData());
    }, []);

    useEffect(() => {
        setTimeout(() => {
            setShowNotif(false);
        }, 3000);
        setTable(() =>
            listingData.map(row => ({
                id: row && "id" in row ? row.id : "",
                avatar: row && "avatar" in row ? row.avatar : "",
                first_name: row && "first_name" in row ? row.first_name : "",
                last_name: row && "last_name" in row ? row.last_name : "",
                email: row && "email" in row ? row.email : ""
            }))
        );
    }, [listingData]);

    return (
        <Container className={classes.container}>
            {!fetching &&
                showNotif &&
                pageNotification &&
                pageNotification.code && (
                    <NotificationManager pageNotification={pageNotification} />
                )}
            {fetching && <LinearProgress />}
            <MaterialTable
                title="Indra CRUD"
                columns={columns}
                data={table}
                icons={tableIcons}
                options={{
                    actionsColumnIndex: -1
                }}
                editable={{
                    onRowAdd: newData =>
                        new Promise(resolve => {
                            resolve();
                            dispatch(addTableRow(newData));
                            setShowNotif(true);
                        }),
                    onRowUpdate: (newData, oldData) =>
                        new Promise(resolve => {
                            resolve();
                            dispatch(updateTableRow(newData));
                            setShowNotif(true);
                        }),
                    onRowDelete: oldData =>
                        new Promise(resolve => {
                            resolve();
                            dispatch(deleteTableRow(oldData));
                            setShowNotif(true);
                        })
                }}
            />
        </Container>
    );
};

App.defaultProps = {
    fetching: false,
    listingData: [],
    pageNotification: null
};

App.propTypes = {
    dispatch: PropTypes.func.isRequired,
    fetching: PropTypes.bool,
    listingData: PropTypes.array,
    pageNotification: PropTypes.oneOfType([PropTypes.object, PropTypes.string])
};

const mapStateToProps = state => ({
    fetching: getFetching(state),
    listingData: getListingData(state),
    pageNotification: getPageNotification(state)
});

export default connect(mapStateToProps)(App);
