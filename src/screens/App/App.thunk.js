import restClient from "../../api/restClient";
import { actions as appActions } from "./App.reducer";
import { showNotification } from "../../utils/NotificationManager/NotificationManager.thunk";

// Resetting
export const resetStore = () => {
    return dispatch => dispatch(resetStarted());
};

const resetStarted = () => ({
    type: appActions.RESET_STATE
});

// Fetching
export const fetchListingData = (page = 1, perPage = 100) => {
    return dispatch => {
        dispatch(startFetchingListingData());
        restClient
            .get("users", {
                params: {
                    page,
                    per_page: perPage
                }
            })
            .then(res => {
                dispatch(fetchingListingDataSuccessful(res.data));
            })
            .catch(err => {
                dispatch(fetchingListingDataFailed(err.response.data));
                dispatch(
                    showNotification({ code: "error", data: err.response.data })
                );
            });
    };
};

const startFetchingListingData = () => ({
    type: appActions.START_FETCHING_LISTING
});

const fetchingListingDataSuccessful = response => ({
    type: appActions.FETCHING_LISTING_SUCCESSFUL,
    response
});

const fetchingListingDataFailed = response => ({
    type: appActions.FETCHING_LISTING_FAILED,
    response
});

// Adding
export const addTableRow = formData => {
    return dispatch => {
        dispatch(startAddingData());

        restClient
            .post("users", formData)
            .then(res => {
                dispatch(addingDataSuccessful(res.data));
                dispatch(showNotification({ code: "added", data: res.data }));
            })
            .catch(err => {
                dispatch(addingDataFailed(err.response.data));
                dispatch(
                    showNotification({ code: "error", data: err.response.data })
                );
            });
    };
};

const startAddingData = () => ({
    type: appActions.START_ADDING_DATA
});

const addingDataSuccessful = response => ({
    type: appActions.ADDING_DATA_SUCCESSFUL,
    response
});

const addingDataFailed = response => ({
    type: appActions.ADDING_DATA_FAILED,
    response
});

// Updating
export const updateTableRow = formData => {
    return dispatch => {
        dispatch(startUpdatingData());

        restClient
            .patch(`users/${formData.id}`, formData)
            .then(res => {
                dispatch(updatingDataSuccessful(res.data));
                dispatch(showNotification({ code: "updated", data: res.data }));
            })
            .catch(err => {
                dispatch(updatingDataFailed(err.response.data));
                dispatch(
                    showNotification({ code: "error", data: err.response.data })
                );
            });
    };
};

const startUpdatingData = () => ({
    type: appActions.START_UPDATING_DATA
});

const updatingDataSuccessful = response => ({
    type: appActions.UPDATING_DATA_SUCCESSFUL,
    response
});

const updatingDataFailed = response => ({
    type: appActions.UPDATING_DATA_FAILED,
    response
});

// Deleting
export const deleteTableRow = formData => {
    return dispatch => {
        dispatch(startDeletingData());

        restClient
            .delete(`users/${formData.id}`)
            .then(res => {
                dispatch(deletingDataSuccessful(formData));
                dispatch(showNotification({ code: "deleted" }));
            })
            .catch(err => {
                dispatch(deletingDataFailed(err.response.data));
                dispatch(
                    showNotification({ code: "error", data: err.response.data })
                );
            });
    };
};

const startDeletingData = () => ({
    type: appActions.START_DELETING_DATA
});

const deletingDataSuccessful = response => ({
    type: appActions.DELETING_DATA_SUCCESSFUL,
    response
});

const deletingDataFailed = response => ({
    type: appActions.DELETING_DATA_FAILED,
    response
});
