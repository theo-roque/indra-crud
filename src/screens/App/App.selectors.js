import { createSelector } from "reselect";

// Check if domain URL params matches the one's in store
export const getListingData = state => state.app.listingData;

export const getFetching = state => state.app.fetching;

export default createSelector([getListingData, getFetching]);
