export const actions = {
    // Reset State
    RESET_STATE: "RESET_STATE",

    // Fetching table data
    START_FETCHING_LISTING: "START_FETCHING_LISTING",
    FETCHING_LISTING_SUCCESSFUL: "FETCHING_LISTING_SUCCESSFUL",
    FETCHING_LISTING_FAILED: "FETCHING_LISTING_FAILED",

    // Adding table data
    START_ADDING_DATA: "START_ADDING_DATA",
    ADDING_DATA_SUCCESSFUL: "ADDING_DATA_SUCCESSFUL",
    ADDING_DATA_FAILED: "ADDING_DATA_FAILED",

    // Updating table data
    START_UPDATING_DATA: "START_UPDATING_DATA",
    UPDATING_DATA_SUCCESSFUL: "UPDATING_DATA_SUCCESSFUL",
    UPDATING_DATA_FAILED: "UPDATING_DATA_FAILED",

    // Deleting table data
    START_DELETING_DATA: "START_DELETING_DATA",
    DELETING_DATA_SUCCESSFUL: "DELETING_DATA_SUCCESSFUL",
    DELETING_DATA_FAILED: "DELETING_DATA_FAILED"
};

export const initialState = {
    error: null,
    listingData: [],
    fetching: false
};

export default (state = initialState, { type, response }) => {
    switch (type) {
        case actions.START_FETCHING_LISTING: {
            return {
                ...state,
                fetching: true
            };
        }
        case actions.FETCHING_LISTING_SUCCESSFUL: {
            return {
                ...state,
                listingData: response.data,
                fetching: false
            };
        }
        case actions.FETCHING_LISTING_FAILED: {
            return {
                ...state,
                error: response
            };
        }
        case actions.START_ADDING_DATA: {
            return {
                ...state,
                fetching: true
            };
        }
        case actions.ADDING_DATA_SUCCESSFUL: {
            const newListingData = [...state.listingData];
            newListingData.push(response);
            return {
                ...state,
                listingData: newListingData,
                fetching: false
            };
        }
        case actions.ADDING_DATA_FAILED: {
            return {
                ...state,
                error: response
            };
        }
        case actions.START_UPDATING_DATA: {
            return {
                ...state,
                fetching: true
            };
        }
        case actions.UPDATING_DATA_SUCCESSFUL: {
            let newListingData = [...state.listingData];
            const index = newListingData.findIndex(x => x.id === response.id);
            newListingData[index] = response;
            return {
                ...state,
                listingData: newListingData,
                fetching: false
            };
        }
        case actions.UPDATING_DATA_FAILED: {
            return {
                ...state,
                error: response
            };
        }
        case actions.START_DELETING_DATA: {
            return {
                ...state,
                fetching: true
            };
        }
        case actions.DELETING_DATA_SUCCESSFUL: {
            let newListingData = [...state.listingData];
            const index = newListingData.findIndex(x => x.id === response.id);
            newListingData.splice(index, 1);
            return {
                ...state,
                listingData: newListingData,
                fetching: false
            };
        }
        case actions.DELETING_DATA_FAILED: {
            return {
                ...state,
                error: response
            };
        }
        default:
            return state;
    }
};
